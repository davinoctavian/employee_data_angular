﻿import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { environment } from '@environments/environment';
import { User } from '@app/_models';
import { Employee } from '@app/_models';
import employees from '../../assets/mock_employees_data.json';

var employeesData = employees

@Injectable({ providedIn: 'root' })
export class AccountService {
    private userSubject: BehaviorSubject<User>;
    private employeeSubject: BehaviorSubject<Employee>;
    public user: Observable<User>;
    public employee: Observable<Employee>;

    constructor(
        private router: Router,
        private http: HttpClient
    ) {
        this.userSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('user')));
        this.user = this.userSubject.asObservable();
        this.employeeSubject = new BehaviorSubject<Employee>(employeesData);
        this.employee = this.employeeSubject.asObservable();
    }

    public get userValue(): User {
        return this.userSubject.value;
    }

    public get employeeValue(): Employee {
        return this.employeeSubject.value;
    }

    login(username, password) {
        return this.http.post<User>(`${environment.apiUrl}/users/authenticate`, { username, password })
            .pipe(map(user => {
                // store user details and jwt token in local storage to keep user logged in between page refreshes
                localStorage.setItem('user', JSON.stringify(user));
                this.userSubject.next(user);
                return user;
            }));
    }

    logout() {
        // remove user from local storage and set current user to null
        localStorage.removeItem('user');
        this.userSubject.next(null);
        this.router.navigate(['/account/login']);
    }

    register(user: User) {
        return this.http.post(`${environment.apiUrl}/users/register`, user);
    }

    getAll() {
        return this.http.get<User[]>(`${environment.apiUrl}/users`);
    }

    getById(id: string) {
        return this.http.get<User>(`${environment.apiUrl}/users/${id}`);
    }

    update(id, params) {
        return this.http.put(`${environment.apiUrl}/users/${id}`, params)
            .pipe(map(x => {
                // update stored user if the logged in user updated their own record
                if (id == this.userValue.id) {
                    // update local storage
                    const user = { ...this.userValue, ...params };
                    localStorage.setItem('user', JSON.stringify(user));

                    // publish updated user to subscribers
                    this.userSubject.next(user);
                }
                return x;
            }));
    }

    delete(id: string) {
        return this.http.delete(`${environment.apiUrl}/users/${id}`)
            .pipe(map(x => {
                // auto logout if the logged in user deleted their own record
                if (id == this.userValue.id) {
                    this.logout();
                }
                return x;
            }));
    }

    //employee
    registerEmployee(employee: Employee) {
        return this.http.post(`${environment.apiUrl}/employees/register`, employee);
    }

    getAllEmployee() {
        return this.http.get<Employee[]>(`${environment.apiUrl}/employees`);
    }

    getEmployeeById(id: string) {
        return this.http.get<Employee>(`${environment.apiUrl}/employees/${id}`);
    }

    updateEmployee(id, params) {
        return this.http.put(`${environment.apiUrl}/employees/${id}`, params)
            .pipe(map(x => {
                // update stored user if the logged in user updated their own record
                if (id == this.employeeValue.id) {
                    // update local storage
                    const employee = { ...this.employeeValue, ...params };
                    employeesData.push(employee)

                    // publish updated user to subscribers
                    this.employeeSubject.next(employee);
                }
                return x;
            }));
    }

    deleteEmployee(id: string) {
        return this.http.delete(`${environment.apiUrl}/employees/${id}`)
            .pipe(map(x => {
                return x;
            }));
    }
}
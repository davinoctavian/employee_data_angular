﻿import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';

import { AccountService, AlertService } from '@app/_services';

@Component({ templateUrl: 'add-edit.component.html' })
export class AddEditComponent implements OnInit {
    form: FormGroup;
    id: string;
    isAddMode: boolean;
    loading = false;
    submitted = false;
    Datenow: Date;
    localDatenow: string;

    constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private accountService: AccountService,
        private alertService: AlertService
    ) {
        this.Datenow = new Date()
        this.localDatenow = this.Datenow.toISOString();
        this.localDatenow = this.localDatenow.substring(0, this.localDatenow.length - 1);
    }

    
    ngOnInit() {
        this.id = this.route.snapshot.params['id'];
        this.isAddMode = !this.id;

        this.form = this.formBuilder.group({
            firstName: ['', Validators.required],
            lastName: ['', Validators.required],
            username: ['', Validators.required],
            email: ['', Validators.required],
            birthDate: ['', Validators.required],
            basicSalary: ['', Validators.required],
            status: ['', Validators.required],
            group: ['', Validators.required],
            description: ['', Validators.required],
            birthDateString: ['', Validators.required],
            descriptionString: ['', Validators.required]
        });

        if (!this.isAddMode) {
            this.accountService.getEmployeeById(this.id)
                .pipe(first())
                .subscribe((x) => {
                    this.form.patchValue(x)
                    if(x.basicSalary){
                        this.formatCurrency(x.basicSalary.toString())
                    }
                    if(x.birthDate) {
                        this.formatDate(x.birthDate, "birthDateString")
                    }
                    if(x.description) {
                        this.formatDate(x.description, "descriptionString")
                    }
                });
        }
    }

    formatCurrency(value) {
        value = value.replace(/Rp/, '').replace(/\./g, '')
        if (value && !isNaN(value)) {
            let num: number = value;
            let result =  new Intl.NumberFormat(['ban', 'id'], {
                style: 'currency',
                currency: 'IDR',
                minimumFractionDigits: 0,
            }).format(Number(num));
        
            this.form.patchValue({basicSalary: result});
        } 
    }

    formatDate(value, par) {
        let localDate
        if (typeof value === 'string') {
            let darr = value.split("/");
            let dateVal = new Date(parseInt(darr[2]),parseInt(darr[1])-1,parseInt(darr[0])+1);
            localDate = dateVal.toISOString();
        }
        else {
            localDate = value.toISOString();
        }
        localDate = localDate.substring(0, localDate.length - 1);
        if (par == "birthDateString") {
            this.form.patchValue({birthDateString: localDate});
        }
        if (par == "descriptionString") {
            this.form.patchValue({descriptionString: localDate});
        }
    }

    numberOnly(event): boolean {
        const charCode = (event.which) ? event.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }

    // convenience getter for easy access to form fields
    get f() { return this.form.controls; }

    onSubmit() {
        this.submitted = true;

        // reset alerts on submit
        this.alertService.clear();
        if(this.form.get("basicSalary").value){
            let valBasicSalary = this.form.get("basicSalary").value.replace(/Rp/, '').replace(/\./g, '')
            this.form.patchValue({basicSalary: valBasicSalary})
        }
        if(this.form.get("birthDateString").value){
            let valBirthDate = new Date(this.form.get("birthDateString").value)
            this.form.patchValue({birthDate: valBirthDate})
        }
        if(this.form.get("descriptionString").value){
            let valDes = new Date(this.form.get("descriptionString").value)
            this.form.patchValue({description: valDes})
        }

        // stop here if form is invalid
        if (this.form.invalid) {
            return;
        }

        this.loading = true;
        
        if (this.isAddMode) {
            this.createEmployee();
        } else {
            this.updateEmployee();
        }
    }

    private createEmployee() {
        this.accountService.registerEmployee(this.form.value)
            .pipe(first())
            .subscribe({
                next: () => {
                    this.alertService.success('Employee added successfully', { keepAfterRouteChange: true });
                    this.router.navigate(['../'], { relativeTo: this.route });
                },
                error: error => {
                    this.alertService.error(error);
                    this.loading = false;
                }
            });
    }

    private updateEmployee() {
        this.accountService.updateEmployee(this.id, this.form.value)
            .pipe(first())
            .subscribe({
                next: () => {
                    this.alertService.success('Update successful', { keepAfterRouteChange: true });
                    this.router.navigate(['../../'], { relativeTo: this.route });
                },
                error: error => {
                    this.alertService.error(error);
                    this.loading = false;
                }
            });
    }
}
﻿import { Component, OnInit, NgModule } from '@angular/core';
import { first } from 'rxjs/operators';

import { AccountService } from '@app/_services';
import {Subject} from 'rxjs';


@Component({ templateUrl: 'list.component.html' })
export class ListComponent implements OnInit {
    employees = null;

    constructor(private accountService: AccountService) {}

    dtOptions: DataTables.Settings = {};
    dtTrigger: Subject<any> = new Subject<any>();
    
    ngOnInit() {
        setTimeout(()=>{  
        this.dtOptions = {
            pagingType: 'full_numbers',
            stateSave: true
            };
        }, 100);
        
        this.accountService.getAllEmployee()
            .pipe(first())
            .subscribe((employees) => {
                this.employees = employees
                this.dtTrigger.next();
            });
    }
    ngOnDestroy(): void {
        this.dtTrigger.unsubscribe();
    }

    deleteEmployee(id: string) {
        const employee = this.employees.find(x => x.id === id);
        employee.isDeleting = true;
        this.accountService.delete(id)
            .pipe(first())
            .subscribe(() => this.employees = this.employees.filter(x => x.id !== id));
    }
}
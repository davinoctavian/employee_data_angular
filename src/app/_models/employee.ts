export class Employee {
    id: string;
    username: string;
    firstName: string;
    lastName: string;
    email: string;
    birthDate: Date;
    basicSalary: Number;
    status: string;
    group: string;
    description: Date;
}